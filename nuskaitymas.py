__author__ = 'Ieva'

import xlrd
############### Transporto priemones ID 2014 m. AR lauke (indeksas 43) ir BR lauke (paskutinis)

def visko_nuskaitymas():
    """ nuskaitomas visas pasirinktas duomenų failas
    reikšmės įrašomos tekstiniame faile
    """
    workbook = xlrd.open_workbook('2013 ATPEIR.xls', encoding_override='UTF-8')
    worksheet = workbook.sheet_by_index(0)

    failas = open("rezultatai2013.txt", 'w')

    temp = []

    for col in range(0, worksheet.ncols):
        pav = worksheet.cell_value(0, col)
        pav = pav.encode('UTF-8')

        failas.write("\n" + pav + ": ")
        for row in range(1, worksheet.nrows):
            reiksme = worksheet.cell_value(row, col)
            if isinstance(reiksme, basestring):
                reiksme = reiksme.encode('UTF-8')
            else:
                reiksme = str(reiksme)

            if reiksme in temp and reiksme != 'Empty' and reiksme != 'Blank':
                continue
            else:
                temp.append(reiksme)
                failas.write(reiksme + "\t\t")
        temp = []
    failas.close()


def duomenu_tipas():
    """ nuskaitomi duomenų failai
    laukų duomenų tipai įrašomi į tekstinį failą stulpeliu vienas po kito
    """
    workbook = xlrd.open_workbook('2013 ATPEIR.xls', encoding_override='UTF-8')
    worksheet = workbook.sheet_by_index(0)
    workbook2 = xlrd.open_workbook('2014_atpeir.xls', encoding_override='UTF-8')
    worksheet2 = workbook2.sheet_by_index(0)
    failas = open("duomenu tipai.txt", 'w')
    array = [None] * worksheet2.ncols
    for row in range(1, worksheet.nrows):
        for col in range(0, worksheet.ncols):
            if worksheet.cell_type(row, col) != 0 and worksheet.cell_type(row, col) != 6 and worksheet.cell_type(0, col) != 0 and worksheet.cell_type(0, col)!=6:
                if col >= 43:
                    col += 1
                    if array[col] == None:
                        array[col] = worksheet.cell_type(row, col-1)
                else:
                    if array[col] == None:
                        array[col] = worksheet.cell_type(row, col)

    for row in range(1, worksheet2.nrows):
        for col in range(0, worksheet2.ncols):
            if worksheet2.cell_type(row, col) != 0 and worksheet2.cell_type(row, col) != 6:
                if array[col] == None:
                    array[col] = worksheet2.cell_type(row, col)

    for i in array:
        if i == 0:
            failas.write("Nepanaudota\n")
        elif i == 1:
            failas.write("Tekstas\n")
        elif i == 2:
            failas.write("Realusis sk.\n")
        elif i == 3:
            failas.write("Data\n")
        elif i == 4:
            failas.write("Loginis\n")
        elif i == None:
            failas.write("Nepanaudotas laukas\n")
        else:
            failas.write("KLAIDA!!!!!!!!!!\n")

    print(str(worksheet.ncols) + ' ' + str(worksheet.nrows))
    print(str(worksheet2.ncols) + ' ' + str(worksheet2.nrows))
    failas.close()


def ivykio_rusis_1():
    """  analizuojami 2-3 stulpeliai, kurie patikslina vienas kitą
    galimos reikšmės įrašomos į tekstinius failus
    """

    #prie worksheet2 stulp pridedamas 1 nuo tolesnio nei 43 indekso

    stulp1 = 46
    stulp2 = 47
    stulp3 = 48

    workbook = xlrd.open_workbook('2013 ATPEIR.xls', encoding_override='UTF-8')
    worksheet = workbook.sheet_by_index(0)
    workbook2 = xlrd.open_workbook('2014_atpeir.xls', encoding_override='UTF-8')
    worksheet2 = workbook2.sheet_by_index(0)
    failas = open("nulemiantys_veiksniai_1.txt", 'w')
    sar = []
    for row in range(1, worksheet.nrows):
        reiksme = worksheet.cell_value(row, stulp1)
        if reiksme not in sar and reiksme != 'Empty' and reiksme != 'Blank' and worksheet.cell_type(row,stulp1)!=0 and worksheet.cell_type(row,stulp1)!=6:
            sar.append(reiksme)
    for row in range(1, worksheet2.nrows):
        reiksme = worksheet2.cell_value(row, stulp1)
        if reiksme not in sar and reiksme != 'Empty' and reiksme != 'Blank' and worksheet2.cell_type(row,stulp1)!=0 and worksheet2.cell_type(row,stulp1)!=6:
            sar.append(reiksme)

    """    2 stulpelio dalis   """

    failas2 = open("nulemiantys_veiksniai_2.txt", 'w')
    sar2 = []
    sar_2viename = [[] for x in range(len(sar))]
    for row in range(1, worksheet.nrows):
        reiksme = worksheet.cell_value(row, stulp2)
        ankstesne_reiksme = worksheet.cell_value(row, stulp1)
        if ankstesne_reiksme == 'Empty' or ankstesne_reiksme == 'Blank' or ankstesne_reiksme == '':
            continue
        if reiksme not in sar_2viename[sar.index(ankstesne_reiksme)] and reiksme != 'Empty' and reiksme != 'Blank'and worksheet.cell_type(row,stulp2)!=0 and worksheet.cell_type(row,stulp2)!=6:
            sar2.append(reiksme)
            sar_2viename[sar.index(ankstesne_reiksme)].append(reiksme)
    for row in range(1, worksheet2.nrows):
        reiksme = worksheet2.cell_value(row, stulp2)
        ankstesne_reiksme = worksheet2.cell_value(row, stulp1)
        if ankstesne_reiksme == 'Empty' or ankstesne_reiksme == 'Blank' or ankstesne_reiksme == '':
            continue
        if reiksme not in sar_2viename[sar.index(ankstesne_reiksme)] and reiksme != 'Empty' and reiksme != 'Blank'and worksheet2.cell_type(row,stulp2)!=0 and worksheet2.cell_type(row,stulp2)!=6:
            sar2.append(reiksme)
            sar_2viename[sar.index(ankstesne_reiksme)].append(reiksme)
    for i, item in enumerate(sar):
        failas.write(item.encode('UTF-8') + "\n")
        #failas.write(str(item) + "\n")
        for k in sar_2viename[i]:
            failas2.write(k.encode('UTF-8') + "\t")
            #failas2.write(str(k) + "\t")
        failas2.write("\n")
    failas2.close()
    failas.close()

    i_lentele2(sar, sar_2viename)

    """   3 stulpelio dalis   """

    sar3 = []
    sar_3viename = [[[] for j in range(len(sar_2viename[x]))] for x in range(len(sar))]
    for row in range(1, worksheet.nrows):
        reiksme = worksheet.cell_value(row, stulp3)
        ankstesne_reiksme = worksheet.cell_value(row, stulp2)
        dar_ankstesne = worksheet.cell_value(row, stulp1)
        if ankstesne_reiksme == 'Empty' or ankstesne_reiksme == 'Blank' or ankstesne_reiksme == '' or dar_ankstesne == '':
            continue
        if reiksme not in sar_3viename[sar.index(dar_ankstesne)][sar_2viename[sar.index(dar_ankstesne)].index(ankstesne_reiksme)] and reiksme != 'Empty' and reiksme != 'Blank'and worksheet.cell_type(row,stulp3)!=0 and worksheet.cell_type(row,stulp3)!=6 and reiksme != '':
            sar3.append(reiksme)
            sar_3viename[sar.index(dar_ankstesne)][sar_2viename[sar.index(dar_ankstesne)].index(ankstesne_reiksme)].append(reiksme)

    for row in range(1, worksheet2.nrows):
        reiksme = worksheet2.cell_value(row, stulp3+1)
        ankstesne_reiksme = worksheet2.cell_value(row, stulp2+1)
        dar_ankstesne = worksheet2.cell_value(row, stulp1+1)
        if ankstesne_reiksme == 'Empty' or ankstesne_reiksme == 'Blank' or ankstesne_reiksme == '' or dar_ankstesne == '':
            continue
        if reiksme not in sar_3viename[sar.index(dar_ankstesne)][sar_2viename[sar.index(dar_ankstesne)].index(ankstesne_reiksme)] and reiksme != 'Empty' and reiksme != 'Blank'and worksheet2.cell_type(row,stulp3+1)!=0 and worksheet2.cell_type(row,stulp3+1)!=6 and reiksme != '':
            sar3.append(reiksme)
            sar_3viename[sar.index(dar_ankstesne)][sar_2viename[sar.index(dar_ankstesne)].index(ankstesne_reiksme)].append(reiksme)


    for i, item in enumerate(sar_2viename):
        file = open("3stulp_" + str(i) + ".txt","w")
        for j in range(len(item)):
            for k in sar_3viename[i][j]:
                file.write(k.encode('UTF-8') + ", ")
            file.write("\n")
        file.close()

    i_lentele3(sar, sar_2viename, sar_3viename)


def i_lentele2(sar, sar_2viename):
    """ Duomenys surašomi lentelės forma į tekstinį failą
    Lentelė sudaroma laukams, kai yra 2 stulpeliai patikslinantys vienas kitą
    """
    failas = open("lentele_nulemiantys_veiksniai.txt", "w")

    """    2 stulpeliu lentele   """

    for i in range(len(sar_2viename)):
        failas.write("\n" + str(i+1) + ".\t" + sar[i].encode('UTF-8') + "\t")
        #failas.write("\n" + str(i+1) + ".\t" + str(sar[i]) + "\t")
        for k in range(len(sar_2viename[i])):
            #failas.write(str(sar_2viename[i][k]) + ", ")

            if k == 0:
                failas.write(sar_2viename[i][k].encode('UTF-8') + ", ")
                #failas.write(str(sar_2viename[i][k]) + ", ")
            elif k != len(sar_2viename[i])-1 and k != 0:
                failas.write(sar_2viename[i][k][:1].lower().encode('UTF-8') + sar_2viename[i][k][1:].encode('UTF-8') + ", ")
            else:
                failas.write(sar_2viename[i][k][:1].lower().encode('UTF-8') + sar_2viename[i][k][1:].encode('UTF-8'))
    failas.close()


def i_lentele3(sar, sar_2viename, sar_3viename):
    """ Duomenys surašomi lentelės forma į tekstinį failą
    Lentelė sudaroma laukams, kai yra 3 stulpeliai patikslinantys vienas kitą
    """
    failas = open("lentele_kiti_ket_pazeidimai.txt", "w")

    """    3 stulpeliu lentele   """

    for i in range(len(sar_3viename)):
        failas.write("\n" + str(i+1) + ".\t" + sar[i].encode('UTF-8') + "\t")
        for j in range(len(sar_3viename[i])):
            if j == 0:
                failas.write(sar_2viename[i][j].encode('UTF-8') + "\t")
            else:
                failas.write("\n\t\t" + sar_2viename[i][j].encode('UTF-8') + "\t")
            for k in range(len(sar_3viename[i][j])):
                if k == 0:
                    failas.write(sar_3viename[i][j][k].encode('UTF-8') + ", ")
                elif k != len(sar_3viename[i][j])-1 and k != 0:
                    failas.write(sar_3viename[i][j][k][:1].lower().encode('UTF-8') + sar_3viename[i][j][k][1:].encode('UTF-8') + ", ")
                else:
                    failas.write(sar_3viename[i][j][k][:1].lower().encode('UTF-8') + sar_3viename[i][j][k][1:].encode('UTF-8'))

    failas.close()


def vienas_stulpelis():
    """ Vieno pasirinkto stulpelio galimų reikšmių įrašymas į tekstinį failą
    """

    workbook = xlrd.open_workbook('2013 ATPEIR.xls', encoding_override='UTF-8')
    worksheet = workbook.sheet_by_index(0)
    workbook2 = xlrd.open_workbook('2014_atpeir.xls', encoding_override='UTF-8')
    worksheet2 = workbook2.sheet_by_index(0)
    file = open("temp.txt", "w")
    stulp = 25
    sar = []

    for row in range(1, worksheet.nrows):
        reiksme = worksheet.cell_value(row, stulp)
        if reiksme not in sar and reiksme != 'Empty' and reiksme != 'Blank' and worksheet.cell_type(row,stulp)!=0 and worksheet.cell_type(row,stulp)!=6:
            sar.append(reiksme)
    for row in range(1, worksheet2.nrows):
        reiksme = worksheet2.cell_value(row, stulp)
        if reiksme not in sar and reiksme != 'Empty' and reiksme != 'Blank' and worksheet2.cell_type(row,stulp)!=0 and worksheet2.cell_type(row,stulp)!=6:
            sar.append(reiksme)

    for i in range(len(sar)):
        if i == 0:
            file.write(sar[i].encode('UTF-8') + ", ")
        else:
            file.write(sar[i][:1].lower().encode('UTF-8') + sar[i][1:].encode('UTF-8') + ", ")
    file.close()

# funkcijos iškviečiamos pagal poreikį analizuojant duomenis
#visko_nuskaitymas()
#duomenu_tipas()
#ivykio_rusis_1()
#vienas_stulpelis()
